# jQuery Stock Tracker

This is a project built for the sake of learning, working with, and employing various data visualization and UI technologies, leveraging finance APIs for data.

##Technologies employed:

* Ruby on Rails/PostgreSQL backend
* Bootstrap CSS Framework for a quickly implemented responsive layout
* Devise (Ruby Gem) for user authentication/account management
* jQueryUI for:
  * Querying/rendering the Quick Stock Lookup (displays relevant suggestions from query response, renders information based on user selection)
  * Panel collapse UI on a user's portfolio
  * Responsive toggleable dropdown nav menu
  * (Work in Progress) Tab interface for past 30 days' and historical YTD price performance of stock on Stocks#show page
* JavaScript (and a Date/Time library) for sanitizing API response dates
* jQuery for:
  * Querying/rendering up-to-date information for stocks on a user's portfolio
* D3.js & D3Plus for:
  * Rendering historical year-to-date price performance and volume of shares traded (two Y-axes, X-axis arranged by trading date)
  * (Work in Progress) Rendering tabbed past 30 days' and historical YTD price performance of stock on Stocks#show page
* jQuery Sparklines for:
  * (Work in Progress) Rendering past 10 day's price performance as a mouse-overable Sparkline on the panel headers of a user's portfolio
  * (Work in Progress) Rendering past 30 day's volume traded as a mouse-overable Sparkline on Stocks#show page
