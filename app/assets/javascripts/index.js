function $updateIndexQuickLook(quote){
	console.log(quote);
	$("#alert").text("You have selected:").removeClass("hide").show("fast");
	$("#add").html(
		"<input type='hidden' value=" + quote.Symbol.toUpperCase() + " name=stock[symbol] >" +
		"<input type='hidden' value=" + quote.Name + " name=stock[name] >");
	$("#selected-symbol-price").html(quote.Symbol + " <small>" +  quote.Ask + "</small>" ).removeClass("hide").show("fast");
	$("#quoteChange").html( " " + quote.Change + " (" + quote.ChangeinPercent + ")" );
		if (quote.Change[0] == "-") {
			$("#quoteChange").css("color","red");
		} else {
			$("#quoteChange").css("color","green");
		};
	$("#selected-name").text(quote.Name).removeClass("hide").show("fast");
	$("#selected-PreviousClose").html(quote.PreviousClose);
	$("#selected-todayHighLow").html(quote.DaysHigh + " / " + quote.DaysLow);
	$("#selected-yearlyHighLow").html(quote.YearHigh + " / " + quote.YearLow);
	$("#selected-AverageDailyVolume").html(quote.AverageDailyVolume);
	$("#selected-stock-data").removeClass("hide").show("fast");
	$("tr:odd").css( "background-color", "#eee" );
}

function beforeAJAXLookup(){
	$("#loading").show();
	$("span.label-info").empty().hide();
}

function getD3Price (stock) {
	return {"date": formattedDate(stock.Date), "value":parseFloat(stock.Close), "name":stock.Symbol};
}

function getD3Volume (stock) {
	return {"date": formattedDate(stock.Date), "volume":parseFloat(stock.Volume), "name":"Volume"};
}
//unused presently
function get12monthlabels(){
	var now = new Date();		
	var labels = [];
	for (var i = 12; i >= 0; i--) {
		var month = now.getMonth() - i
		var dateToArray = new Date(new Date().setMonth(month));
		labels.push(dateToArray.format("mmmm"));
	}
	return labels
}

function $oneYearHistoryChartViz(data){
		//chart here from mapped results
		var labels = get12monthlabels();
	  var visualization = d3plus.viz(data)
	    .container("#viz")  
	    .data(data)       
	    .type("line")      
	    .id("name")         
	    .text("name")       
	    .legend({"align": "start"})
	    .y({'value': "value",
	    		'label': "Value (per hare in USD)"})					
	    .y2({'value': "volume",
	    			'label': "Volume of Shares Traded"})
	    .x({'grid': false,
	    		'value': "date",
	    		'label': "Last 365 days (Daily)"}) 
	    .draw()             
}

function $oneYearHistoryAJAXlookup(symbol){
	$.ajax({
		url: "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.historicaldata%20where%20symbol%20%3D%20%22"
			+ symbol + "%22%20and%20startDate%20%3D%20%22"
			+ oneYearPastDateForAPIquery()
			+ "%22%20and%20endDate%20%3D%20%22" 
			+ currentDateForAPIquery() 
			+ "%22&format=json&diagnostics=true&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback="
	}).success( function(response){
		var historicalData = response.query.results.quote
		var D3Price = historicalData.map(getD3Price)
		var D3Volume = historicalData.map(getD3Volume)
		var result = D3Price.concat(D3Volume) 
		$oneYearHistoryChartViz(result);
	});
}

function $renderIndexQuickLookup(response){
	var quote = response.query.results.quote
	$updateIndexQuickLook(quote);
	$oneYearHistoryAJAXlookup(quote.symbol);
}

function $IndexQuickLookupAndRender(symbol){
$.ajax({
	url: "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22"
				+ symbol + "%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=",
	dataType: "json"
	}).success( function(response){
		$renderIndexQuickLookup(response)
	});				
}

$( document ).ready(function() {

	$("#searchform").on("submit", function(event){
		event.preventDefault();
	})

	$("#symbolsearch")
	.focus()
	.autocomplete({
		source: function(request,response) {
			$.ajax({
				beforeSend: function(){ 
					beforeAJAXLookup();
				},
				url: "http://dev.markitondemand.com/api/v2/Lookup/jsonp",
				dataType: "jsonp",
				data: { input: request.term
				},
				success: function(data) {
					response( $.map(data, function(item) {
						return {
							label: item.Name + "/" + item.Symbol + " (" +item.Exchange+ ")",
							symbol: item.Symbol,
							name: item.Name,
							exchange: item.Exchange
						}
					}));
					$("#loading").hide();
				}
			});
		},
		minLength: 0,
		select: function( event, ui ) {
			$("#viz").empty();
			var symbol = ui.item.symbol.toUpperCase();
			$IndexQuickLookupAndRender(symbol)
		}
	});
});