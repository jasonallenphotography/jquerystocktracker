function $updateUserShowQuickLook(quote){
	var symbol = quote.Symbol
	$(".selected-symbol" + symbol).html(quote.Symbol + " <small>" +  quote.Ask + "</small>" ).removeClass("hide").show("fast");
	$(".quoteChange" + symbol).html( " " + quote.Change + " (" + quote.ChangeinPercent + ")" );
		if (quote.Change[0] == "-") {
			$(".quoteChange" + symbol).css("color","red");
		} else {
			$(".quoteChange" + symbol).css("color","green");
		};
	$(".selected-name" + symbol).html(quote.Name);
	$(".selected-PreviousClose"+ symbol).html(quote.PreviousClose);
	$(".selected-todayHighLow"+ symbol).html(quote.DaysHigh + " / " + quote.DaysLow);
	$(".selected-yearlyHighLow"+ symbol).html(quote.YearHigh + " / " + quote.YearLow);
	$(".selected-AverageDailyVolume"+ symbol).html(quote.AverageDailyVolume);
	$(".selected-stock-data"+ symbol).removeClass("hide").show("fast");
	$("tr:odd").css( "background-color", "#eee" );
}

function $renderUserShowQuickLookup(response){
	var quote = response.query.results.quote
	$updateUserShowQuickLook(quote);
}

function $UserShowQuickLookupAndRender(symbol){
$.ajax({
	url: "https://query.yahooapis.com/v1/public/yql?q=select%20*%20from%20yahoo.finance.quotes%20where%20symbol%20in%20(%22"
				+ symbol + "%22)&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys&callback=",
	dataType: "json"
	}).success( function(response, symbol){
		$renderUserShowQuickLookup(response,symbol)
	});				
}

$(document).ready(function(){


})