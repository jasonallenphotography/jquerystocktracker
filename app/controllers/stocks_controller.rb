class StocksController < ApplicationController
  before_action :set_stock, only: [:show, :edit, :update, :destroy]
  before_action :set_user

  # GET /stocks/1
  def show
  end

  # GET /stocks/1/edit
  def edit
  end

  # POST /stocks
  def create
    @stock = Stock.new(stock_params)
    @stock.user = current_user
    if @stock.save
      redirect_to @user, notice: "#{@stock.symbol} was added to your stocks."
    else
      @errors = @stock.errors.full_messages
      render :new
    end
  end

  # PATCH/PUT /stocks/1
  def update
    if @stock.update(stock_params)
      redirect_to @user, notice: "#{@stock.symbol} was successfully updated."
    else
      @errors = @stock.errors.full_messages
      render :edit
    end
  end

  # DELETE /stocks/1
  def destroy
    @stock.destroy
    redirect_to @user, notice: "#{@stock.symbol} was removed from your stocks"
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_stock
      @stock = Stock.find(params[:id])
    end

    def set_user
      @user = current_user
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def stock_params
      params.require(:stock).permit(:name,:symbol) 
    end
end
