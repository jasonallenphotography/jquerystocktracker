class UsersController < ApplicationController
before_action :find_user, :redirect_unless_owner_of_page_is_current_user

	def show
		userstocks = @user.stocks
		@stocks = userstocks.paginate(:page => params[:page], :per_page => 10)
	end

private
	
	def find_user
		@user = User.find(params[:id])
	end

	def redirect_unless_owner_of_page_is_current_user
		unless current_user == @user
			@errors = ["You may only view your own profile"]
			render 'home/index'
		end
	end
end
